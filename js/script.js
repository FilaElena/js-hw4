//1. Дан массив с элементами [1, 2, 3, 4, 5]. С помощью цикла for выведите все эти
//элементы на экран.

const arr1 = [1, 2, 3, 4, 5];

for (let i = 0; i <= 4; i++) {
    console.log (arr1[i]);
}


// 2. Дан массив с числами [-2, -1, -3, 15, 0, -4, 2, -5, 9, -15, 0, 4, 5, -6, 10, 7]. Числа могут
// быть положительными и отрицательными. Выведите на экран только отрицательные
// числа, которые больше -10, но меньше -3.

const arr2 = [-2, -1, -3, 15, 0, -4, 2, -5, 9, -15, 0, 4, 5, -6, 10, 7];

for (let i = 0; i < arr2.length; i++) {

    if (arr2[i] > -10 && arr2[i] < -3 ) {
        console.log (arr2[i]);
    }
}


// 3. Создайте новый массив и заполните его значениями от 23 до 57, используя цикл for и
// while. Выведите оба массива. С помощью цикла for найдите сумму элементов этого
// массива. Запишите ее в переменную result и выведите.

let arr3 = [];

for (let i = 23; i <= 57; i++) {
    arr3.push (i);
}
console.log (arr3);

let result = 0;

for (let i = 0; i < arr3.length; i++) {
    result += arr3[i];
}

console.log (`сумма элементов массива ${result}`);


// 4. Дан массив числами (строчного типа), например: [‘10’, ‘20’, ‘30’, ‘50’, ‘235’, ‘3000’].
// Выведите на экран только те числа из массива, которые начинаются на цифру 1, 2 или
// 5.

const arr4 = ['10', '20', '30', '50', '235', '3000', '222'];

for (let i =0; i < arr4.length; i++) {
    var arr4Str = arr4[i];
    if (arr4Str[0] == 1 || arr4Str[0] == 2 || arr4Str[0] == 5) {
        console.log (arr4Str);
    }
}


// 5. Составьте массив дней недели (ПН, ВТ, СР и т.д.). С помощью цикла for выведите все
// дни недели, а выходные дни выведите жирным.

const arr5 = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВСКР'];

for (let i = 0; i < 7; i++) {

    if (arr5[i] === 'СБ' || arr5[i] === 'ВСКР') {
        document.write (`<p style = "font-weight:bold">${arr5[i]}</p>`);
    } else {
        document.write (`<p>${arr5[i]}</p>`);
    }

}


// 6. Создайте массив с произвольными данными. Добавьте в конец массива любой элемент,
// и получите последний элемент массива, используя свойство length.

let arr6 = [3, 7, 87, 93, 812, 65];
arr6.push ('Слива');
console.log (arr6);
console.log (arr6[arr6.length - 1]);


// 7. Запросите у пользователя по очереди числовые значения при помощи prompt и
// сохраните их в массив. Собирайте числа до тез пор пока пользователь не введет пустое
// значение. Выведите получившийся массив на экран. Выполните сортировку чисел
// массива, и выведите его на экран.

let arr7 = [];

for ( ; ; ) {
    var a = +prompt ('Введите число');
    if (isNaN(a) === true || a == ' ') {  //почему при вводе 0 тоже происходит break??
        break;
    }

    arr7.push (a);
}

document.write (`<p>${arr7}</p>`);

let arr7Sort = arr7.sort (function(a, b) {
     return a - b; 
});

document.write (`<p>${arr7Sort}</p>`);


// 8. Переверните массив [12, false, ‘Текст’, 4, 2, -5, 0] (выведите в обратном порядке),
// используя цикл while и метод reverse.

let arr8 = [12, false, 'Текст', 4, 2, -5, 0];

// let reverse = [];
// for (let i = 0; i < arr8.length; i++) {
//     reverse.push (arr8[arr8.length - 1 - i]);
// }

// console.log (reverse);

arr8.reverse();
console.log (arr8);


// 9. Напишите скрипт, считающий количество нулевых (пустых) элементов в заданном
// целочисленном массиве [5, 9, 21, , , 9, 78, , , , 6].

let arr9 = [5, 9, 21, , , 9, 78, , , , 6,];
let count = 0;

for (i = 0; i < arr9.length; i++) {
    if (arr9[i] == null) {
        count++;
    }
}

console.log (count);


// 10. Найдите сумму элементов массива между двумя нулями (первым и последним нулями
//     в массиве). Если двух нулей нет в массиве, то выведите ноль. В массиве может быть
//     более 2х нулей. Пример массива: [48,9,0,4,21,2,1,0,8,84,76,8,4,13,2] или
//     [1,8,0,13,76,8,7,0,22,0,2,3,2].

let arr10 = [1,8,0,13,76,8,7,0,22,0,2,3,2];
let sum = 0;

let zeroFirst = arr10.indexOf (0);
let zeroSecond = arr10.lastIndexOf(0);

if (zeroFirst == zeroSecond) {
    console.log (0);
} else {
    for (i = zeroFirst; i < zeroSecond; i++ ) {
        sum += arr10[i];
    }
    console.log (`сумма элементов массива ${sum}`);
}






// 11. *** Нарисовать равнобедренный треугольник из символов ^. Высоту выбирает
// пользователь. Например: высота = 5, на экране






// let height = 5;
 
// for (let i = 1; i <= height; i++) {
// for (let j = 1; j <= height - i; j++) {
//     document.write("&nbsp;"); 
// }  
// for (let k = 1; k <= 2 * i - 1; k++) {
//     document.write('^'); 
// }  
// document.write('<br>');  
// }













